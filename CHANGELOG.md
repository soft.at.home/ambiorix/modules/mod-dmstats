# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.5 - 2024-09-11(15:59:24 +0000)

### Other

- - [Tr181-logical] Logical Manager Interface Statistics are not getting updated from NetDev statistics

## Release v0.1.4 - 2024-09-09(07:03:48 +0000)

### Fixes

- mod-dmstats answers wrongly when a read action on "Stats" object is done.

### Other

- [mod-dmstats] dmstats wrongly answer an empty object path when a _get is done on a parameter of upper object with infinite depth

## Release v0.1.3 - 2022-06-17(09:06:59 +0000)

### Fixes

- [ambiorix] dmstats return variant contains the object not the parameter in the get request

## Release v0.1.2 - 2022-05-20(06:36:10 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

### Other

- Move component from ambiorix to core on gitlab.com

## Release v0.1.1 - 2021-09-17(13:30:11 +0000)

### Other

- Change name from stats-object to mod-dmstats
- Opensource component

## Release v0.1.0 - 2021-09-03(11:41:15 +0000)

### New

- HOP-301 Initial implementation of stats object

