 
# Module Datamodel Interface Statistics

[[_TOC_]]

## Introduction

In the [Broadband Forumn TR-181 data model definition](https://usp-data-models.broadband-forum.org/tr-181-2-14-0-usp.html) often interface statisics are defined. These statistics are often related to network interfaces. Some other data model plug-ins are providing these statisics and implement fetching these statistics. 

Using the bus system it is easy to fetch these and use them in an other data model.

This module implements fetching these network interface statisics and makes them available in your data model.

## Building, Installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

Please read [Getting Started](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md) to create a local workspace and to set-up a local development environment.

More information can be found at:

- [Debug & development environment](https://gitlab.com/prpl-foundation/components/ambiorix/dockers/amx-sah-dbg/-/blob/main/README.md) to set-up a local development environment.

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc) - Generic C api for common data containers
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp) - Common patterns implementations
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo) - The ODL compiler library
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd) - Data model C API
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb) - Bus Agnostic API
- [libsahtrace](https://gitlab.com/soft.at.home/logging/libsahtrace) - Logging library

### Build mod-dmstats

1. Clone the git repository

    To be able to build it, you need the source code. So clone this git-repo to your workspace

    ```bash
    mkdir -p ~/development/amx
    cd ~/development/amx
    git clone git@gitlab.com:prpl-foundation/components/core/modules/mod-dmstats.git
    ``` 

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building. Install all libraries needed. When you created the local workspace and followed the [Getting Started](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md) guide, everything should be installed.

    As an alternative it is possible to installed pre-compiled packages in the development container using:

    ```bash
    sudo apt update
    sudo apt install libamxc libamxd libamxp libamxo libamxb sah-lib-sahtrace-dev
    ```

1. Build it

    ```bash
    cd ~/development/amx/mod_dmstats
    make
    ```

1. Install it

    When the build is successful, install the binary at the correct location.

    ```bash
    sudo -E make install
    ```

## Using module data model interface statistics

### Import the module

In your main odl file `import` the shared object of this module.

```odl
%config {
    name = "test";

    import-dbg = true;

    // main files
    definition_file = "${name}_definition.odl";
}

import "mod-dmstats.so";

include "${definition_file}";
```

The statics are fetched from `netdev-plugin` so it is also recommended to add the keyword `requires` in your odl file.


```odl
%config {
    name = "test";

    import-dbg = true;

    // main files
    definition_file = "${name}_definition.odl";
}

requires "NetDev.";
import "mod-dmstats.so";

include "${definition_file}";
```

After including it, all data model extension functions will be available for use.

### Define A Statisics Object

An network interface statisics object can be added with any name to your data model.

The parent of the statistics object must contain a parameter with name `NetDevName` or `Name`, and is used as a reference. If both are defined, then `NetDevName` will be used.

To make the statisics object work, the following actions must be overriden.

- read - use method `stats_object_read`
- list - use method `stats_object_list`
- describe - use method `stats_object_describe`

The parameters added under the statisics object are:

- BytesSent
- BytesReceived
- PacketsSent
- PacketsReceived
- ErrorsSent
- ErrorsReceived
- UnicastPacketsSent
- UnicastPacketsReceived
- DiscardPacketsSent
- DiscardPacketsReceived
- MulticastPacketsSent
- MulticastPacketsReceived
- BroadcastPacketsSent
- BroadcastPacketsReceived
- UnknownProtoPacketsReceived

Example:

```odl
%define {
    object MyInterface[] {
        string Name;
        object Stats {
            on action read call stats_object_read;
            on action list call stats_object_list;
            on action describe call stats_object_describe;
        }
    }

}

%populate {
    object MyInterface {
        instance add(0,"") {
            parameter Name = "lo";
        }

        instance add(0,"") {
            parameter NetDevName = "eth0";
        }
    }
}
```

In the above example a mult-instance object `MyInterface` is defined. Each instance created will contain a `Stats` object and a `NetDevName` or `Name` parameter. If both are defined, then `NetDevName` will be used.

In the `populate` section, two instances are created. When reading the `Stats` object of these instances, this module will fetch the statistics from netdev-plugin and uses this search path:

```
NetDev.Link.[Name == '<NAME>'].Stats
```

From the returned statisics only these are used:

- TxBytes -> BytesSent
- RxBytes -> BytesReceived
- TxPackets -> PacketsSent
- RxPackets -> PacketsReceived
- TxErrors -> ErrorsSent
- RxErrors -> ErrorsReceived